Day 1 (28/5/2022)
1. Revise JDBC 
2. Go through the readmes -- 


Day 2 (30/5/2022)
1. import day2.1 , in your eclipse (new workspace). Fix build path n change user name n password in DBUtils.
1.1 Revise CRUD application with employee.
1.2 Revise invocation n execution of stored procedure.

2. Follow the steps 
2.1 core java help : day1_help \installation instructions
Ensure that you are working with JDK 11 n STS 3.9.18

2.2 Refer to -- day2-data\day2_help\web server help\Updated steps in tomcat.txt
Ensure 
Apache Tomcat 9.0.36 is added n running fine in your IDE (advanced Java workspace)
Create a new dynamic web project (: day2_web_lab , as per the steps) n test HTML content , using chrome browser.
Stop web server(Tomcat)

Hands on

3. Refer to day2-data\day2_help\case studies\topic-tutorials.png

3.1 Refer to day1-data\day1_help\jdbc help\sql\topics-tutorials.txt"
Copy DDL n DML for : users , topics n tutorials table.

3.2 
Solve 
Create new java project : day2_assignment .
Add my sql connector in build path.

Layered app 
Tester ---DAO --- DBUtils (openCn , getCn , closeCn) --POJO/s --DB 

Solve  
1. User Login
i/p : email ,password
o/p :Return User details in case of successful login or an error  message, in case of invalid login.

Hint :  IUserDao : i/f
User authenticateUser(String email,String pwd) throws SQLException
Steps
1. Table -- users
2. POJO cls -- User
properties --id | name    | email          | password | reg_amt | reg_date   | role 
Def ctor , all getters n setters , toString

2.5 DBUtils ---openCn , closeCn , getCn

3. DAO :
3.1 IUserDao : i/f
User authenticateUser(String email,String pwd) throws SQLException
In case of successful login --ret User object , populated with use details lifted from DB
o.w --null
3.2 UserDaoImpl
state --conn , pst1 
def ctor ---getCn , pst1=.....
cleanUp -- close psts
CRUD : set IN params , execQuery --RST --if(rst.next()) --- ret user details else ret null

4. Tester
init --sc, openCn, create dao
service --call CRUD method
destroy --cleanUP Dao , close cn



2. Change password
i/p : email ,password , new password
o/p : A message indicating success or failure.

Hint :  IUserDao : i/f
String changePassword(email ,password , new password) throws SQLException

3. Get all available topics.
Hint : ITopicDao
List<Topic> getAllTopics() throws SE.

Day 3
Please follow this exact sequence in today's lab , for complete revision n clarity.

Refer to today's sequence.txt , diagrams , readmes for better understanding n revise.

NOTE : Use web browser's inspect option , to trace request response flow.

Steps
1. Create a new dynamic web project (check web.xml checkbox) : eg day3_lab

1.1 Create a welcome page : index.html
Add a link : to invoke the servlet (which will be deployed using @WebServlet annoatation)

1.1 Request URL  sent from clnt --> server, after clicking of the link
http://host:port/day3_lab/test1
URI : /day3_lab/test1
URL Pattern : /test1


Create FirstServlet.java , override life cycle methods & trace the life cycle.
Use @WebServlet annoatation
Use lazy loading policy.

1.2 Add another link in index.html : to invoke the servlet (which will be deployed using xml tags)

Create SecondServlet.java , override life cycle methods & trace the life cycle.
Do NOT Use @WebServlet annoatation , instead add xml tags in web.xml
Use eager loading policy.

-----------------------------------------
What will happen ?

1.3 Add / in anchor tag's href. Observe n conclude
URL : http://localhsot:8080/test1
Resp :HTTP 404

1.4 Remove / from the url-pattern . Observe n conclude
Obs : WC (Server side JVM) throws : IllegalArgumentException --- invalid URL pattern !!!!!!!!!!!!


1.5 Give same url-pattern to 2 different servlets . Observe n conclude
Obs : Web server simply DOES NOT start
WC (Server side JVM) throws : IllegalArgumentException ---multiple servlets CAN NOT have SAME url-pattern




1.6 Can one servlet be deployed under multiple url patterns -- YES 


Common Troubleshooting Tips
1. Stop web server
2. R Click on Tomcat server --- add n remove --Remove All : This will un deploy all web apps from the server.
3. R Click on Tomcat server --clean
4. R Click on Tomcat server --clean tomcat work directory
5. Start server (If it starts correctly => no server issues!)
6. Clean Project n deploy the dynamic web project n test it.





1.7 Add a new link in index.html : for User Login
Copy login.html from day3-data\day3_help\HTML pages\login.html under src\main\webapp(i.e context root of the web application)
Write a LoginServlet 
override init , do???? n destroy 
Perform user authentication.
